package br.org.oracle.concurrency;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

public class Parallel {

    public static void main(String[] args) {

//        concurrentAccessOnVector();
        sameBehaviorButThreadSafe();
//        parallelProcessingUndetermined();
//        forkAndJoin();


//      CyclicBarrier cyclicBarrier = new CyclicBarrier(10, () -> System.out.println("foi!"));
//
//      IntStream.iterate(1, i -> i++).limit(9).forEach(i -> ava(cyclicBarrier));

//        System.out.println(i1 + " " + i2);

//        Stream<String> parallel = Stream.of("1", "2", "3").parallel();
//
//        ConcurrentMap<Boolean, List<String>> data =
//                Stream.of(parallel, parallel)
//                        .flatMap(s -> s)
//                        .collect(Collectors.groupingByConcurrent(s -> !s.startsWith("1")));
    }

    private static void ava(CyclicBarrier cyclicBarrier) {
        try {
            cyclicBarrier.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (BrokenBarrierException e) {
            e.printStackTrace();
        }
    }

    private static void forkAndJoin() {
        ForkJoinPool forkJoinPool = new ForkJoinPool(1);
        ForkJoinTask<?> task = new MyTask();
        Object invoke = forkJoinPool.invoke(task);
    }

    public static class MyTask extends RecursiveAction {
        @Override
        protected void compute() {
            invokeAll(new MyTask(), new MyTask());
        }
    }

    private static void parallelProcessingUndetermined() {
        Integer i1 = Arrays.asList(1, 2, 3, 4).stream().findAny().get();

        synchronized (i1) {
            Integer i2 = Arrays.asList(5, 6, 7, 8)
                    .parallelStream()
                    .sorted()
                    .findAny().get();
            System.out.println(i1 + " " + i2);
        }
    }

    private static void sameBehaviorButThreadSafe() {
        List<Integer> l1 = Arrays.asList(1, 2, 3);
        List<Integer> l2 = new CopyOnWriteArrayList<>(l1);

        Set<Integer> s3 = new ConcurrentSkipListSet<>();
        s3.addAll(l1);

        for (Integer item : l2) l2.add(4);
        for (Integer item : s3) {
            System.out.print(item);s3.add(5);};
        System.out.println();
        System.out.println(l1.size() + " " + l2.size() + " " + s3.size());
    }

    private static void concurrentAccessOnVector() {
        AtomicLong value1 = new AtomicLong(0);
        final long[] value2 = {0};

        IntStream.iterate(1, i -> 1).limit(100).parallel().forEach(i -> value1.incrementAndGet());
        IntStream.iterate(1, i -> 1).limit(100).parallel().forEach(i -> ++value2[0]);

        System.out.println(value1 + " " + value2[0]);
    }
}
