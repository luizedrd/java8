package br.org.oracle.concurrency;

import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class SingleThreadExecutor {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        test_invokeAll();
        test_invokeAny();
    }

    private static void test_invokeAll() throws InterruptedException {

        ExecutorService executorService = Executors.newSingleThreadExecutor();

        List<Task> tasks = IntStream.range(1, 10).mapToObj(Task::new).collect(Collectors.toList());

        List<Future<String>> futures = executorService.invokeAll(tasks);

        futures.forEach(f -> System.out.println(f.isDone()));

        executorService.shutdown();
    }

    private static void test_invokeAny() throws ExecutionException, InterruptedException {
        ExecutorService executorService = Executors.newSingleThreadExecutor();

        List<Task> tasks = IntStream.range(1, 10).mapToObj(Task::new).collect(Collectors.toList());

        String future = executorService.invokeAny(tasks);

        System.out.println(future);

        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

}
