package br.org.oracle.concurrency;

import java.time.LocalTime;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ScheduledThreadExecutor {

    public static void main(String[] args) throws InterruptedException, ExecutionException {

//        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(2);
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
//
        test_scheduleAtFixedRate(executorService);
//        test_scheduleAtFixedDelay();

    }

    private static void test_scheduleAtFixedRate(ScheduledExecutorService scheduledExecutorService) {

        long initialDelay = 0;
        long rate = 2;

        System.out.println("Starting : " + LocalTime.now());

        System.out.println("Job delayed to " + initialDelay + " seconds");

        scheduledExecutorService.scheduleAtFixedRate(new Job("A", 3), initialDelay, rate, TimeUnit.SECONDS);
        scheduledExecutorService.scheduleAtFixedRate(new Job("B", 3), initialDelay, rate, TimeUnit.SECONDS);

        try {
            Thread.sleep(12000);
        } catch (Exception e) {
        }

        scheduledExecutorService.shutdown();
    }

    private static void test_scheduleAtFixedDelay(ScheduledExecutorService scheduledExecutorService) {

        Job job = new Job(1);

        long initialDelay = 0;
        long rate = 2;

        System.out.println("Starting : " + LocalTime.now());

        System.out.println("Job delayed to " + initialDelay + " seconds");

        scheduledExecutorService.scheduleWithFixedDelay(job, initialDelay, rate, TimeUnit.SECONDS);

        try {
            Thread.sleep(20000);
        } catch (Exception e) {
        }

        scheduledExecutorService.shutdown();
    }
}
