package br.org.oracle.concurrency;

import java.util.concurrent.Callable;

public class Task implements Callable<String> {

    private int i;

    Task(int i) {
        this.i = i;
    }

    public Task() {

    }

    @Override
    public String call() throws Exception {

        System.out.println("Call was called for: " + i);

        if (i < 3) Thread.sleep(1000);

        return "The end for: " + i;
    }
}
