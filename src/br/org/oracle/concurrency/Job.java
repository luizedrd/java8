package br.org.oracle.concurrency;

import java.time.LocalTime;

public class Job implements Runnable {

    private String nome;
    private int i;

    Job() {
    }

    Job(int runningTime) {
        this.i = runningTime;
    }

    Job(String nome, int runningTime) {
        this.nome = nome;
        this.i = runningTime;
    }

    @Override
    public void run() {
        LocalTime init = LocalTime.now();
        try {


            Thread.sleep(i * 1000);
        } catch (Exception e) {

        }
        System.out.println("Running job " + nome + " at " + init + " and finished at " + LocalTime.now());

    }
}
