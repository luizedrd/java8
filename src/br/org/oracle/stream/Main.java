package br.org.oracle.stream;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.*;
import java.util.stream.*;

@SuppressWarnings("all")
public class Main {

    public static void main(String[] args) {

//        Consumer consumer = new Consumer<T>() {
//            @Override
//            public void accept(T t) {
//
//            }
//        };

        Consumer<Integer> consumer = (x) -> System.out.println(x);

        System.out.println(Stream.empty());

        Stream.empty().forEach(System.out::println);

        System.out.println("\nArray to stream");

        Stream.of("a", 1, 0x1).forEach(System.out::println);

        System.out.println("\nIterate stream");
        Stream.iterate(1, n -> n + 1).limit(3).forEach(System.out::print);

        System.out.println("\nList to stream");
        List<Integer> integers = Arrays.asList(1, 2, 3);
        integers.stream().forEach(System.out::print);


        System.out.print("\nCount: ");
        System.out.print(Stream.of(1, 2, 3).count());

        System.out.print("\nMin: ");
        Optional<Integer> min = Stream.of(1, 2, 3).min(Comparator.comparingInt(a -> a));
        System.out.print(min);

        System.out.print("\nMax: ");

        Optional<String> max = Stream.of("aba", "abac", "abacate").min((a, b) -> b.length() - a.length());
        max.ifPresent(System.out::print);

        System.out.print("\nFindAny: ");
        Stream.of("aba", "abac", "abacate").findAny().ifPresent(System.out::print);

        System.out.print("\nFindFirst: " + Stream.empty().findFirst());

        System.out.print("\nSort Desc: ");
        Stream<String> sortedStream = Stream.of("aba/", "abacate/", "abac/").sorted((a, b) -> b.length() - a.length());
        sortedStream.forEach(System.out::print);

        System.out.print("\nStream reduce: " + Stream.of("aba/", "abacate/", "abac/").reduce("1", (s, c) -> s + c));
        System.out.print("\nStream reduce1: " + Stream.of(BigDecimal.ONE, BigDecimal.TEN).reduce((s, c) -> s.add(c)));

        Stream<Integer> objectStream = Stream.of(1).filter(s -> s % 2 == 0);
        System.out.print("\nStream reduce empty List with seed: " + objectStream.reduce(1, (a, b) -> a * b));
        objectStream = Stream.of(1).filter(s -> s % 2 == 0);
        System.out.print("\nStream reduce empty List without seed: " + objectStream.reduce((a, b) -> a * b));

//        List<BigDecimal> list = Collections.emptyList();
//        Stream.of(list);
//        System.out.print("\nStream reduce1: " + Stream.of(list).reduce(new BigDecimal(99), (s, c) -> s.add(c)));

        System.out.print("\nCollect from stream: ");
//        Stream.of("aba", "abac", "abacate").collect(StringBuilder::new, (sb, s) -> {sb.append(s).append(",");}, StringBuilder::append); // NOKs
//        StringBuilder collected = Stream.of("aba", "ca", "te").collect(StringBuilder::new, (sb, s) -> sb.append(s), (sb, s) -> sb.append(s));
//        System.out.println(collected);
        Stream.of("aba", "ca", "te").collect(StringBuilder::new, (sb, s) -> sb.append(s).append(","), (StringBuilder sb, StringBuilder sb2) -> {
            System.out.println(sb.toString());
            System.out.println(sb2.toString());
        });

        Stream.of("aba", "ca", "te").collect(Collectors.toCollection(ArrayList::new));

        System.out.print("\nmap values: ");
        Stream.of("aba", "ca", "te").map(s -> !s.contains("a") ? 0 : s).forEach((s) -> System.out.print(s + ", "));

        System.out.print("\nstream peek values: ");
        AtomicInteger i = new AtomicInteger();
        Stream.of("aba", "ca", "te").peek(s -> System.out.print(" " + (i.incrementAndGet()) + ":")).forEach(System.out::print);

        class AbcValues {
            double valor;
            String desc;

            @Override
            public String toString() {
                return "AbcValues{" +
                        "valor=" + valor +
                        ", desc='" + desc + '\'' +
                        '}';
            }
        }

        System.out.print("\ndouble stream mapped to object: ");
        DoubleStream.of(12.1, 13.2, 14.2).mapToObj(new DoubleFunction<Object>() {
            @Override
            public Object apply(double value) {
                AbcValues abcValues = new AbcValues();
                abcValues.desc = "Segundo valor";

                abcValues.valor = value;
                return abcValues;
            }
        }).skip(1).limit(1).forEach(System.out::print);

        System.out.print("\nmapping values: ");
        Stream.of("aba", "ca", "te").map(String::length).forEach(System.out::print);
        Stream.of("aba", "ca", "te").map(Main::countString).forEach(System.out::print);


        Optional<String> aba = Optional.of("aba");
        Optional<Integer> optionalInteger = aba.map(Main::countString); //OK
        //Optional<Integer> optionalInteger2 = aba.map(Main::countStringO); //NOK

        Optional<Integer> optionalInteger2 = aba.flatMap(Main::countStringO); //OK

        IntStream intStream = IntStream.rangeClosed(1, 5);

        LongStream longStream = intStream.mapToLong(x -> x);

        // OK
//        OptionalDouble average = longStream.average();
//        Double doubleValor = average.isPresent() ? average.getAsDouble() : average.orElse(0);

        System.out.print("\nlong summary statistics: ");
        LongSummaryStatistics longSummaryStatistics = longStream.summaryStatistics();
        System.out.println(longSummaryStatistics);

        Stream<String> aba1 = Stream.of("aba", "ca", "te");
        System.out.println("Find first " + aba1.findFirst().get() + ". Find Any: " + aba1.findAny().get());
    }

    private static Optional<Integer> countStringO(String s) {
        return Optional.ofNullable(s.length());
    }

    private static Integer countString(String s) {
        return s.length();
    }


}
