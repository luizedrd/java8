package br.org.oracle.date;

import java.util.ListResourceBundle;

public class Zoo_pt extends ListResourceBundle {

    @Override
    protected Object[][] getContents() {
        return new Object[][]{
                {"hello", "hello"},
                {"open", "The zoo is open"},
                {"a", new Integer("1234")}
        };
    }
}
