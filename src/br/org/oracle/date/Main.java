package br.org.oracle.date;

import java.util.ListResourceBundle;
import java.util.Locale;
import java.util.Properties;
import java.util.ResourceBundle;

public class Main {

    public static void main(String[] args) {


        System.out.println(Locale.getDefault());

        System.out.println(Locale.GERMAN);
        System.out.println(Locale.GERMANY);

        System.out.println(new Locale("xx", "PT"));

        Locale build = new Locale.Builder().setLanguage("xx").setRegion("US").build();
        System.out.println(build);

        Locale localeUS = new Locale("en", "US");
        Locale localeFR = new Locale("fr", "FR");


        ResourceBundle zoo = ResourceBundle.getBundle("Zoo", localeUS);
        ResourceBundle zooFR = ResourceBundle.getBundle("Zoo", localeFR);

        System.out.println(zoo.getString("hello"));
        System.out.println(zooFR.getString("hello"));

        Locale pt = new Locale("pt");
        ResourceBundle resourceBundle = ResourceBundle.getBundle("date.Zoo", pt);
        System.out.println(pt);
        System.out.println(resourceBundle.getObject("a"));
//       System.out.println(resourceBundle.getString("a"));
    }

}
