package br.org.oracle.optional;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.function.Supplier;

class Main {

    static Supplier<BigDecimal> supplier = ()-> BigDecimal.ZERO;

    static Supplier<Exception> exceptionSupplier = ()-> new IllegalArgumentException("Nao aceito");

    public static void main(String[] args) throws Exception {

        Optional<BigDecimal> valor = Optional.of(BigDecimal.ONE);

        System.out.println(valor.orElse(BigDecimal.TEN));

        valor = Optional.ofNullable(valor.get());

        System.out.println(valor.isPresent() ? valor.orElseGet(supplier) : valor.orElseThrow(exceptionSupplier));
    }
}
